/**
 * Задание 1.
 *
 * Получить и вывести в консоль следующие элементы страницы:
 * - По идентификатору (id): элемент с идентификатором list;
 * - По классу — элементы с классом list-item;
 * - По тэгу — элементы с тэгом li;
 * - По CSS селектору (один элемент) — третий li из всего списка;
 * - По CSS селектору (много элементов) — все доступные элементы li.
 *
 * Вывести в консоль и объяснить свойства элемента:
 * - innerText;
 * - innerHTML;
 * - outerHTML.
 */
let byId = document.getElementById('list');
let byClassName = document.getElementsByClassName('list-item');
let byTagName = document.getElementsByTagName('li');
let querySelector = document.querySelector('li:nth-of-type(3)');
let querySelectorAll = document.querySelectorAll('li');

console.log('byId', byId);
console.log('byClassName', byClassName);
console.log('byTagName', byTagName);
console.log('querySelector', querySelector);
console.log('querySelectorAll', querySelectorAll);

console.log('innerText: \n', byId.innerText);
console.log('innerHTML: \n', byId.innerHTML);
console.log('outerHTML: \n', byId.outerHTML);
