// Теоретический вопрос
// 1. Функции нужны что бы не писать один и тот же код много раз (повторяющееся действие).
// 2. Если у функции есть хоть один параметр - то в неё нужно передать хотя бы один аргумент,
//     что бы она выполнилась. Параметр - переменная, аргумент - её значение.


function calc (firstArg, secondArg, operator) {
    firstArg = +prompt('Enter first number:');
    secondArg = +prompt('Enter second number:');
    operator = prompt('Enter operator (+, -, *, /):');
    if (operator === '+') {
        return firstArg + secondArg;
    } else if (operator === '-') {
        return firstArg - secondArg;
    } else if (operator === '*') {
        return firstArg * secondArg;
    } else if (operator === '/') {
        return firstArg / secondArg;
    } else {
        return 'Wrong input';
    }
}
console.log(calc());