// Экранирование это способ вставить в строчный эллемент или сообщение спецсимвол,
// что бы он не воспринимался как часть кода. Выполняется при помощи "\"
// перед экранируемым символом.

function createNewUser() {
    let firstName = prompt('Enter your First Name:');
    let lastName = prompt('Enter your Last Name:');
    let bd = prompt('Enter your age (dd.mm.yyyy):')
    return newUser = {
        firstName,
        lastName,
        bd: bd,
        getLogin() {
           return (firstName[0] + lastName).toLowerCase();
        },
        getAge() {
            let dd = bd.slice(0, 2);
            let mm = bd.slice(3, 5);
            let yyyy = bd.slice(6, 10);
            return (new Date().getTime() - new Date(yyyy, mm, dd)) / (24 * 3600 * 365.25 * 1000) | 0;
        },
        getPassword() {
            return firstName[0].toUpperCase() + lastName.toLowerCase() + bd.slice(6, 10);
        }
    }
}
createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
