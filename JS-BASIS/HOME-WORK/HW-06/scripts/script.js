// Метод forEach работает как цикл, который перебирает каждый элемент массива
// и выполняет для него указанное действие.

const arr = ['hello', 'world', 23, '23', null];
function filterBy(arr, type) {
    return arr.filter(n => typeof n !== `${type}`);
}
console.log(filterBy(arr, 'string'));
console.log(filterBy(arr, 'number'));
// var a2 = a1.filter(function(item) { return typeof item == 'number'; });