// Теоретический вопрос
// 1. Циклы нужны для того что бы повторить определенное действие необходимое количество раз,
//     до соблюдения условия, при котором цикл прекращается.

let endNumber = +prompt('Enter number to count to:');
for (let i = 0; i <= endNumber; i++) {
    if (isNaN(endNumber) || endNumber < 5) {
        console.log('Sorry, no numbers');
    } else if (i % 5 === 0 && i !== 0) {
        console.log(i);
    }
}
