// Теоретический вопрос:
// 1. Переменная объявленная через var объявленная локально - видна внутри всей функции,
// а через let - только внутри функциональных блоков кода в фигурных скобках, где она объявлена.
// При глобальном объявлении (вне функции или блока) - большой разницы нет.
// const объявляется в тех случаях, когда значение изначально задано и не меняется в дальнейшем
// или вычисляется один раз и дальше не меняется.
// 2. Ранее в js были частыми ошибки переприсвоения значения переменных,
// объявленых как var, при их повторной декларации, let в таком случае выдаст ошибку,
// что позволяет проще контролировать содержание переменной.

let userConfirm;
let userName = prompt('Enter your name:');
let userAge = +prompt('Enter your age:');
if (userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if (userAge >= 18 && userAge <= 22) {
    userConfirm = confirm('Are you sure you want to continue?');
    if (userConfirm === true) {
        alert('Welcome ' + userName + '!');
    } else {
        alert('You are not allowed to visit this website.');
    }
} else if (userAge > 22) {
    alert('Welcome ' + userName + '!');
} else {
    alert('Wrong input!');
}
